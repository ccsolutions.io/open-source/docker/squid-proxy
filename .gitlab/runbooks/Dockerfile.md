# Runbook for Squid Dockerfile v1.0.0

This runbook provides detailed steps on how to build and run a Docker image based on the provided Squid Dockerfile.

## Step 1: Prerequisites

Make sure that you have Docker installed on your system. If you haven't, follow the official [Docker installation guide](https://docs.docker.com/engine/install/) for your specific operating system.

## Step 2: Cloning the Repository

Clone the repository containing the Dockerfile. Replace `[repository_url]` with the URL of your repository.

```bash
git clone [repository_url]
cd [cloned_repository_directory]
```

## Step 3: Building the Docker Image

To build the Docker image, navigate to the directory containing the Dockerfile. Replace `[dockerfile_directory]` with the path to your Dockerfile.

Then, run the following command:

```bash
docker build -t squid_image --build-arg SQUID_VERSION=5.2-22.04_beta --build-arg TIMEZONE=Europe/Berlin [dockerfile_directory]
```

This will build a Docker image tagged as `squid_image`. It uses the provided Squid version (5.2-22.04_beta) and sets the timezone to 'Europe/Berlin'.

## Step 4: Running the Docker Container

After successfully building the image, you can run the Docker container using the following command:

```bash
docker run -d squid_image
```

This command will run the Docker container in the background.

## Step 5: Monitoring the Squid Logs

You can check the logs of the running Docker container by first finding the container ID with `docker ps` and then using `docker logs`. Replace `[container_id]` with your actual container ID.

```bash
docker logs [container_id]
```

## Support

If you encounter any issues or require support while following this runbook, please contact us at [devops@ccsolutions.io](mailto:devops@ccsolutions.io).
