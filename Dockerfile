ARG SQUID_VERSION
FROM ubuntu/squid:${SQUID_VERSION}

MAINTAINER CCSolutions.io UG <devops@ccsolutions.io>

ARG TIMEZONE
ENV TZ=${TIMEZONE}

RUN apt update \
    && apt upgrade -y \
    && rm -rf /var/lib/apt/lists/*

ADD config/squid.conf /etc/squid/squid.conf

RUN ln -sf /dev/stdout /var/log/squid/access.log \
    && ln -sf /dev/stderr /var/log/squid/error.log
