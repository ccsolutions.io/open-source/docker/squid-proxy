# Docker-Squid

This is a Dockerfile to build a squid proxy server on a Docker container. The Dockerfile is created and maintained by [CCSolutions.io UG](mailto:devops@ccsolutions.io).

This docker container will update and upgrade the operating system and squid proxy server to the latest version and then configure squid according to your config file.

## Features

- Custom Squid version
- Timezone specification
- Configurable Squid settings
- Logs piped to stdout and stderr for easier debugging

## How to Use this Dockerfile

1. **Specify Squid Version**

   You can specify the version of Squid proxy server you want to use with the `SQUID_VERSION` ARG at build time.

   Example:
   ```
   docker build --build-arg SQUID_VERSION=4.6 .
   ```

2. **Set Your Timezone**

   The Docker container uses the system timezone by default. You can specify a custom timezone with the `TIMEZONE` ARG at build time.

   Example:
   ```
   docker build --build-arg TIMEZONE=Europe/Berlin .
   ```

3. **Custom Squid Configuration**

   You can add your own custom `squid.conf` file for Squid to the `config/` directory.

4. **Build the Docker Image**

   Run the following command in the directory of your Dockerfile to build the Docker image:
   ```
   docker build -t your-image-name .
   ```

5. **Run the Docker Container**

   To run the Docker container, execute the following command:
   ```
   docker run -d --name your-container-name your-image-name
   ```

## Logs

The logs for the Squid proxy server are linked to stdout and stderr. This allows you to use the `docker logs` command to easily view the logs.

Example:
```
docker logs your-container-name
```

## Support

For support or any questions, please reach out to us at [devops@ccsolutions.io](mailto:devops@ccsolutions.io).

**Note:** Always be sure to follow best practices when configuring your Squid proxy server to ensure security and performance. If you need help with this, consider reaching out to a professional or consultant.

## License

This project is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT).
